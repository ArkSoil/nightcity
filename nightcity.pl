#!/bin/perl


use strict;
use warnings;
use Image::Magick;
use Data::Dumper;

$| = 1;

#global read only
our $SCALE = 3;
our ($IMG_WIDTH, $IMG_HEIGHT) = (200,200);

#TODO:
#vertical windows
#water mirror
#roof decorations
#better sky
#fix scale 1



#ideas
#time date and location from data
#weather (sunny, snow, rain) from data (be carefull to not mess up data)

#every building contains a whole number of bits, the rest is empty

#add pipe functionality
#add small file error "your file is too small for even a single building, pathetic", if <42 bytes (if header describes more keep rest at 0)
sub get_data{
	my ($fn,$width,$offset) = @_;
	open my $file, "<:raw", $fn;
	my $data = '';
	seek $file,$offset,0;
	read $file,$data,$width;
	close $file;
	
	my @data_a = split "", $data;
	return @data_a;
}

sub get_header{
	my ($fn,$offset) = @_;
	#xx000000 00000000 - width 5-9
	#00xxxx00 00000000 - heigth 8-24
	#000000x0 00000000 - window style (horizontal, vertical)
	#0000000x 00000000 - roof decoration
	#00000000 xx000000 - decoration type
	#00000000 00xx0000 - decoration color
	#00000000 0000xx00 - decoration variant
	#00000000 000000xx - building offset
	
	#xxx00000 00000000 - width 5-20
	#000xxxxx 00000000 - heigth 8-39
	#00000000 xxx00000 - building offset -2 - 5
	#00000000 000x0000 - window style (horizontal, vertical)


	#every bulding have 2 sides, data on the other side is transformed from other
	my @data_a = get_data($fn,2,$offset);
	my ($a,$b) = @data_a;
	($a, $b) = (ord($a),ord($b));
	my %header = (
		width => ($a >> 5)+5,
		height => ($a & 31)+8,
		offset => ($b >> 5) -2,
		w_style => 1#($b >> 4) & 1,
	);
	return %header;
}

sub draw_row{
	my ($img,$rand,$x,$y,$length,$color,$orientation) = @_;
	for(my $i = 0;$i<$length;$i+=1){
		my ($x1, $x2, $y1, $y2) = ($x,$x,$y,$y);
		if(~$orientation){
			($x1, $x2, $y2) = (
				$x+$SCALE*($i+1)+1,
				$x+$SCALE*($i+2),
				$y+$SCALE-1
			);
		}else{
			($x2, $y1, $y2) = (
				$x+$SCALE-1,
				$y+$SCALE*($i+1)+1,
				$y+$SCALE*($i+2)	
			);
		} 
		if($rand & (1 << $i)){
			$img->Draw(
				fill => $color,
				points => "$x1,$y1,$x2,$y2",
				primitive => 'rectangle'
			);
		}
	}
}

sub draw_row2{
	my ($img,$rand,$x,$y,$length,$color) = @_;
	for(my $i = 0;$i<$length*2-1;$i+=1){
		my ($x1, $x2, $y1, $y2) = (
			$x,
			$x+$SCALE-1,
			$y+$SCALE*($i+1)+1,
			$y+$SCALE*($i+2)
		);
		if($rand & (1 << $i)){
			$img->Draw(
				fill => $color,
				points => "$x1,$y1,$x2,$y2",
				primitive => 'rectangle'
			);
		}
	}
}

sub draw_bulding{
	my ($img, $x, $rows, $columns) = @_;
	my $height = ($rows*2+1)*$SCALE;
	my $width = ($columns+2)*$SCALE;
	my ($x1,$y1,$x2,$y2) = ($x+$width,$IMG_HEIGHT-$height,$x+$width*2,$IMG_HEIGHT);
	$img->Draw(
		fill => '#222222',
		points => "$x1,$y1,$x2,$y2",
		primitive => 'rectangle'
	);
	($x1,$y1,$x2,$y2) = ($x,$IMG_HEIGHT-$height,$x+$width,$IMG_HEIGHT);
	$img->Draw(
		fill => '#111111',
		points => "$x1,$y1,$x2,$y2",
		primitive => 'rectangle'
	);
	for(my $i = 0;$i<$rows;$i+=1){
		draw_row($img,int(rand(512)),$width+$x,$IMG_HEIGHT-$height+$i*$SCALE*2+$SCALE,$columns,"#ffffff");
	}
	for(my $i = 0;$i<$rows;$i+=1){
		draw_row($img,int(rand(512)),$x,$IMG_HEIGHT-$height+$i*$SCALE*2+$SCALE,$columns,"#aaaaaa");
	}

}

sub draw_bulding2{
	my ($img, $x, $rows, $columns,$w_style, @data) = @_;
	my $height = ($rows*2+1)*$SCALE;
	my $width = ($columns+2)*$SCALE;
	my ($x1,$y1,$x2,$y2) = ($x+$width,$IMG_HEIGHT-$height,$x+$width*2,$IMG_HEIGHT);
	$img->Draw(
		fill => '#222222',
		points => "$x1,$y1,$x2,$y2",
		primitive => 'rectangle'
	);
	($x1,$y1,$x2,$y2) = ($x,$IMG_HEIGHT-$height,$x+$width,$IMG_HEIGHT);
	$img->Draw(
		fill => '#111111',
		points => "$x1,$y1,$x2,$y2",
		primitive => 'rectangle'
	);
	if($w_style){
	#vertical
	for(my $i = 0;$i<int($columns/2 + ($columns % 2));$i+=1){
		print $data[$i], "\n";
		draw_row2(
			$img,
			~$data[@data-$i-1],
			$x+($i*2+1+$columns+2)*$SCALE,
			$IMG_HEIGHT-$height,
			$rows,
			"#ffffff",
			$w_style
		);
		draw_row2(
			$img,
			$data[$i],
			$x+($i*2+1)*$SCALE,
			$IMG_HEIGHT-$height,
			$rows,
			"#aaaaaa",
			$w_style
		);
	}}else{
	#horisontal
	for(my $i = 0;$i<$rows;$i+=1){
		draw_row(
			$img,
			~$data[@data-$i-1],
			$width+$x,
			$IMG_HEIGHT-$height+$i*$SCALE*2+$SCALE,
			$columns,
			"#ffffff",
			$w_style
		);
		draw_row(
			$img,
			$data[$i],
			$x,
			$IMG_HEIGHT-$height+$i*$SCALE*2+$SCALE,
			$columns,
			"#aaaaaa",
			$w_style
		);
	}}

}

sub draw_bulding3{
	my ($img, $x, $rows, $columns, @data) = @_;
	my $height = ($rows*2+1)*$SCALE;
	my $width = ($columns+2)*$SCALE;
	my ($x1,$y1,$x2,$y2) = ($x+$width,$IMG_HEIGHT-$height,$x+$width*2,$IMG_HEIGHT);
	$img->Draw(
		fill => '#222222',
		points => "$x1,$y1,$x2,$y2",
		primitive => 'rectangle'
	);
	($x1,$y1,$x2,$y2) = ($x,$IMG_HEIGHT-$height,$x+$width,$IMG_HEIGHT);
	$img->Draw(
		fill => '#111111',
		points => "$x1,$y1,$x2,$y2",
		primitive => 'rectangle'
	);
	#print Dumper(@data);
	for(my $i = 0;$i<int($columns/2 + ($columns % 2));$i+=1){
		draw_row2(
			$img,
			int(rand(2**($rows*2))),
			$x+($i*2+1+$columns+2)*$SCALE,
			$IMG_HEIGHT-$height,
			$rows,
			"#ffffff"
		);
		draw_row2(
			$img,
			int(rand(2**($rows*2))),
			$x+($i*2+1)*$SCALE,
			$IMG_HEIGHT-$height,
			$rows,
			"#aaaaaa"
		);
	}

}

sub convert_data{
	my ($width, @data) = @_;	
	my @c_data;
	my $data_cnt = 0;
	my $buffer = 0;
	my $size = 0;
	while($data_cnt < @data || $width <= $size){
		if($size < $width){
			$buffer = ($buffer << 8) + ord($data[$data_cnt]);
			$size += 8;
			$data_cnt += 1;
		}else{
			push @c_data,($buffer >> $size-$width);
			$buffer = $buffer & (2**($size-$width)-1);
			$size -= $width;
		}
	}
	if($size != 0){
		$buffer = $buffer << $size;
		push @c_data, $buffer >> $size-$width;
	}
	return @c_data;
	
}

sub draw_buldings{
	my ($img,$fn) = @_;
	my $offset = 0;
	my $data_offset = 0;
	while($offset < $IMG_WIDTH){
	
		my %h = get_header($fn,$data_offset);
		my $size = $h{'height'}*$h{'width'};
		my $data_size = int($size/8);
		my @data = get_data($fn,$data_size,$data_offset+2);
		my @c_data;
		if($h{'wstyle'}){
			@c_data = convert_data($h{'height'},@data);
		}else{
			@c_data = convert_data($h{'width'},@data);
		}
		draw_bulding2($img,$offset-$h{offset},$h{'height'},$h{'width'},$h{w_style},@c_data);
		$offset += (($h{width}+2)*2-$h{offset})*$SCALE;
		$data_offset += $data_size+2;
	}

}

sub check_length{
	my ($fn) = @_;
	my $offset = 0;
	my $data_offset = 0;
	
	my $f_size = (stat $fn)[7];

	while($data_offset < $f_size){
		my %h = get_header($fn,$data_offset);
		my $size = $h{'height'}*$h{'width'};
		my $data_size = int($size/8);
		$offset += (($h{width}+2)*2-$h{offset})*$SCALE;
		$data_offset += $data_size+2;
	}
	print $offset;
}

my $img=Image::Magick->new;
$img->Set(size=>'200x200');
$img->ReadImage('gradient:purple-magenta3');

draw_buldings($img,"./nightcity.pl");


#draw_bulding3($img,0,16,9,int(rand(65536)));

#check_length("./nightcity.pl");

$img->Write('test.png');
